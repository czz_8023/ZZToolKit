//
//  ZZBannerViewCtrl.swift
//  ZZToolKitDemo
//
//  Created by Czz on 2022/6/23.
//

import UIKit
import ZZToolKit

extension UIView{
    func bgColor(_ color: UIColor) -> Self{
        self.backgroundColor = color
        return self
    }
}

class ZZBannerViewCtrl: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let banner1 = bannerView()
        banner1.1.itemSpace = 20
        banner1.1.contentInset = UIEdgeInsets(top: 0, left: 30, bottom: 10, right: 40)
        banner1.1.bannerArr = ["icon_timeout","icon_timeout","icon_timeout",]
        
        
        let banner2 = bannerView()
        banner2.1.inset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        banner2.1.bannerArr = ["icon_timeout","icon_timeout","icon_timeout",]
        
        let banner3 = bannerView()
        banner3.1.inset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 10)
        banner3.1.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 40)
        banner3.1.bannerArr = ["icon_timeout","icon_timeout","icon_timeout",]
        
        let banner4 = bannerView()
        banner4.1.itemSpace = 20
        banner4.1.bannerArr = ["icon_timeout","icon_timeout","icon_timeout",]
        
        
        let banner5 = bannerView()
        banner5.1.itemSpace = 20
        banner5.1.customViews = [UILabel().bgColor(.yellow).text("这是一个自定义View").textAlignment(.center),
                                 UIView().bgColor(.blue),
                                 UIView().bgColor(.orange)]
        banner5.1.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    
        
        let items = [banner1.0,banner2.0,banner3.0,banner4.0, banner5.0]
        
        let zzScrollView = ZZScrollView(items: items)
        zzScrollView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(zzScrollView)
        zzScrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        zzScrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        zzScrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        zzScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    func bannerView() -> (ZZScrollView.Item, ZZBannerView) {
        let view = ZZBannerView()
        view.createBannerImagesBlock = { page, imageView, object in
            imageView.backgroundColor = UIColor.lightGray
            imageView.cornerRadius(10)
            imageView.contentMode = .scaleAspectFill
        }
        view.isAutoRun = true
        view.isCycleRunBanner = true
        view.masksToBounds(true)
        view.backgroundColor = .red
        
        let item = ZZScrollView.Item(view: view, inset: UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0), minHeight: 150)
        
        return (item, view)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
