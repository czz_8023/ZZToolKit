//
//  ViewController.swift
//  ZZToolKitDemo
//
//  Created by Czz on 2022/6/23.
//

import UIKit
import ZZToolKit

class ViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        let thisDateStr = "2022-11-19 05:00:01"
//        let thisDate = Date.date(with: thisDateStr, formatter: "yyyy-MM-dd HH:mm:ss")!
//        let totalDays = thisDate.monthTotalDays()
//        for day in 1 ... totalDays {
//            let dayStr = day < 10 ? "0\(day)" : "\(day)"
//            let dateStr = "\(thisDate.year)-\(thisDate.month)-\(dayStr)"
//            let date = Date.date(with: dateStr, formatter: "yyyy-MM-dd")
//
//            ZZLog("\(date?.year ?? "") - \(date?.chineseMonth ?? "") - \(date?.chineseDay ?? "")     \(dateStr)")
//        }
//
//        ZZLog("时辰  \(thisDate.chineseHour)")
        
        let jsonString = """
                            {"name" : "张三", "age": 18, "sex":1, "address": "address1"}
                        """
        
        let jsonStringArr = """
                            [{"name" : "张三1", "age": 18, "sex":1, "address": "address1"},
                            {"name" : "张三2", "age": 181, "sex":2, "address": "address2"},
                            {"name" : "张三3", "age": 182, "sex":3, "address": "address3"},
                            {"name" : "张三4", "age": null, "sex":1, "address": null},
                            {"name" : "张三5", "age": 184, "sex":1, "address": "address5"}]
                            """
        
        let model = ZZTool.jsonDecode(StudentMode.self, json: jsonString)
        ZZLog(model?.name)
        
        
        let model2 = ZZTool.jsonDecode([StudentMode].self, json: jsonStringArr)
        ZZLog(model2?.first?.name)
        
        
    }

    
    enum Sex : Int, Codable {
        case man = 1
        case woman = 2
        case unknow = 3
    }
    
    struct StudentMode: Codable {
        var name: String?
        var age: Int? = 0
        var sex: Sex?
        var address: String?
    }

}

