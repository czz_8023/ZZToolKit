//
//  ZZUIButtonViewCtrl.swift
//  ZZToolKitDemo
//
//  Created by Czz on 2022/6/23.
//

import UIKit
import ZZToolKit

class ZZUIButtonViewCtrl: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        
        let items = [
                     
                     ZZScrollView.Item(view: UILabel().text("下面是修改contentAlignment"), minHeight: 80),
                     button(title: "LeftCenter", contentAlignment: .LeftCenter, imageAlignment: .LeftCenter),
                     button(title: "RightCenter", contentAlignment: .RightCenter, imageAlignment: .LeftCenter),
                     button(title: "TopCenter", contentAlignment: .TopCenter, imageAlignment: .LeftCenter),
                     button(title: "BottomCenter", contentAlignment: .BottomCenter, imageAlignment: .LeftCenter),
                     button(title: "TopLeft", contentAlignment: .TopLeft, imageAlignment: .LeftCenter),
                     button(title: "TopRight", contentAlignment: .TopRight, imageAlignment: .LeftCenter),
                     button(title: "BottomLeft", contentAlignment: .BottomLeft, imageAlignment: .LeftCenter),
                     button(title: "BottomRight", contentAlignment: .BottomRight, imageAlignment: .LeftCenter),
                     
                     button(title: "Center", contentAlignment: .Center, imageAlignment: .LeftCenter),
                     button(title: "Left", contentAlignment: .Left, imageAlignment: .LeftCenter),
                     button(title: "Top", contentAlignment: .Top, imageAlignment: .LeftCenter),
                     button(title: "Right", contentAlignment: .Right, imageAlignment: .LeftCenter),
                     button(title: "Bottom", contentAlignment: .Bottom, imageAlignment: .LeftCenter),
                     
                     ZZScrollView.Item(view: UILabel().text("下面是修改imageAlignment"), minHeight: 80),
                     
                     button(title: "LeftCenter", contentAlignment: .Center, imageAlignment: .LeftCenter),
                                  button(title: "RightCenter", contentAlignment: .Center, imageAlignment: .RightCenter),
                                  button(title: "TopCenter", contentAlignment: .Center, imageAlignment: .TopCenter),
                                  button(title: "BottomCenter", contentAlignment: .Center, imageAlignment: .BottomCenter),
                                  button(title: "TopLeft", contentAlignment: .Center, imageAlignment: .TopLeft),
                                  button(title: "TopRight", contentAlignment: .Center, imageAlignment: .TopRight),
                                  button(title: "BottomLeft", contentAlignment: .Center, imageAlignment: .BottomLeft),
                                  button(title: "BottomRight", contentAlignment: .Center, imageAlignment: .BottomRight),
                                  
                                  button(title: "Center", contentAlignment: .Center, imageAlignment: .Center),
                                  button(title: "Left", contentAlignment: .Center, imageAlignment: .Left),
                                  button(title: "Top", contentAlignment: .Center, imageAlignment: .Top),
                                  button(title: "Right", contentAlignment: .Center, imageAlignment: .Right),
                                  button(title: "Bottom", contentAlignment: .Center, imageAlignment: .Bottom),
        
        ]
        
        let zzScrollView = ZZScrollView(items: items)
        zzScrollView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(zzScrollView)
        zzScrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        zzScrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        zzScrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        zzScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    func button(title: String, contentAlignment: ZZUIButton.Alignment, imageAlignment: ZZUIButton.Alignment ) -> ZZScrollView.Item {
        let button = ZZUIButton()
        button.backgroundColor(color: .red, state: .normal)
            .backgroundColor(color: .blue, state: .highlighted)
            .backgroundColor(color: .orange, state: .selected)
            .backgroundColor(color: .yellow, state: .disable)
            .title(title: "normal", state: .normal)
            .title(title: "highlighted", state: .highlighted)
            .title(title: "selected", state: .selected)
            .title(title: "disable", state: .disable)
            .image(image: UIImage.name("icon_timeout"), state: .normal)
            .title(title: title, state: .normal)
            .contentAlignment(contentAlignment)
            .imageAlignment(imageAlignment)
            .space(20)
            .contentViewBgColor(color: .green, state: .normal)
            .contentInset(UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        
        let item = ZZScrollView.Item(view: button, inset: UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0), minHeight: 100, fixedWidth: 200)
        
        return item
    }
}
