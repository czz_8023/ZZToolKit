//
//  ZZScrollViewCtrl.swift
//  ZZToolKitDemo
//
//  Created by Czz on 2022/6/23.
//

import UIKit
import ZZToolKit
import WebKit

class ZZScrollViewCtrl: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let view1 = UIView()
        view1.backgroundColor = UIColor.red
        
        let view2 = UILabel()
        view2.text = "我是固定宽度的Label"
        view2.backgroundColor = UIColor.orange
        
        let webView = WKWebView()
        webView.load(URLRequest(url: Url(url: "https://www.baidu.com")!))
        
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = CGSize(width: 100, height: 80)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(cell: TestCollectionItem.self)
        
        
        let items = [ZZScrollView.Item(view: view1, inset: UIEdgeInsets(top: 20, left: 10, bottom: 20, right: 0), minHeight: 200),
                     ZZScrollView.Item(view: webView, inset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)),
                     ZZScrollView.Item(view: tableView, inset: UIEdgeInsets(top: 20, left: 10, bottom: 20, right: 50), minHeight: 200),
                     ZZScrollView.Item(view: view2, inset: UIEdgeInsets(top: 10, left: 50, bottom: 10, right: 10), minHeight: 100, fixedWidth: 200),
                     ZZScrollView.Item(view: collectionView, inset: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 10)),]
        
        let zzScrollView = ZZScrollView(items: items)
        zzScrollView.backgroundColor = UIColor.blue
        zzScrollView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(zzScrollView)
        zzScrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        zzScrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        zzScrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        zzScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }

}

extension ZZScrollViewCtrl: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 44
    }
}

extension ZZScrollViewCtrl: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.defaultCell()
        cell.textLabel?.text = "TableView\(indexPath)"
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
}


extension ZZScrollViewCtrl: UICollectionViewDataSource, UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.cell(cellClass: TestCollectionItem.self, indexPath: indexPath)
        cell.textLabel.text = "CollectionView\(indexPath)"
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}


fileprivate class TestCollectionItem: FatherCollectionViewCell {
    lazy var textLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func createView() {
        contentView.addSubview(textLabel)
        textLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        textLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        textLabel.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        textLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
    }
}
