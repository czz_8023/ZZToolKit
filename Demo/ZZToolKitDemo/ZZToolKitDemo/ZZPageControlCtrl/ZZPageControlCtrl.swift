//
//  ZZPageControlCtrl.swift
//  ZZToolKitDemo
//
//  Created by Czz on 2022/6/23.
//

import UIKit
import ZZToolKit

class ZZPageControlCtrl: UIViewController {
    
    private var currPage = 0{
        didSet{
            zzScrollView.items.forEach { item in
                if item.view is ZZPageControl{
                    let pageView = item.view as? ZZPageControl
                    pageView?.currentPage = currPage
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let items = [pageCtrol(alignment: .Left),
                     pageCtrol(alignment: .Center),
                     pageCtrol(alignment: .Right),
                     
                     pageCtrol(alignment: .Left, pageImage: nil, currentPageImage: nil, pageIndicatorTintColor: .red, currentPageIndicatorTintColor: .blue),
                     pageCtrol(alignment: .Center, pageImage: nil, currentPageImage: nil, pageIndicatorTintColor: .red, currentPageIndicatorTintColor: .blue),
                     pageCtrol(alignment: .Right, pageImage: nil, currentPageImage: nil, pageIndicatorTintColor: .red, currentPageIndicatorTintColor: .blue),
                     ZZScrollView.Item(view: self.changeBtn, inset: UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10))]
        
        zzScrollView.items = items;

        view.addSubview(zzScrollView)
        zzScrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        zzScrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        zzScrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        zzScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    
    func pageCtrol(alignment: ZZPageControl.PageControlAlignment,
                   pageImage: UIImage? = UIImage.name("icon_timeout")?.reSizeImage(reSize: CGSize(20)),
                   currentPageImage: UIImage? = UIImage.image(color: .orange, size: CGSize(20)),
                   pageIndicatorTintColor: UIColor? = nil,
                   currentPageIndicatorTintColor: UIColor? = nil) -> ZZScrollView.Item {
        let view = ZZPageControl()
        view.numberOfPages = 8
        view.currentPage = 0
        view.pageImage = pageImage
        view.currentPageImage = currentPageImage
        view.alignment = alignment
        view.pageIndicatorTintColor = pageIndicatorTintColor
        view.currentPageIndicatorTintColor = currentPageIndicatorTintColor
        view.pageSpace = 10
        view.indicatorSize = CGSize(width: 20, height: 15)
        view.backgroundColor = .green
        
        let item = ZZScrollView.Item(view: view, inset: UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10), minHeight: 30)
        
        return item
    }
    
    lazy var zzScrollView: ZZScrollView = {
        let view = ZZScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var changeBtn: ZZUIButton = {
        let btn = ZZUIButton(normal: "添加")
            .backgroundColor(color: .orange, state: .normal)
            .backgroundColor(color: .blue, state: .highlighted)
            .titleLabel({$0.textColor(.red).font(UIFont.systemFont(ofSize: 20, weight: .bold))})
            .titleColor(color: .white, state: .normal)
            .addBlock(for: .touchUpInside, block: { [weak self] sender in
                guard let `self` = self else { return }
                self.currPage += 1;
                if (self.currPage >= 8) {
                    self.currPage = 0
                }
            })
            .height(55)
            .cornerRadius(10)
        return btn
    }()
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
