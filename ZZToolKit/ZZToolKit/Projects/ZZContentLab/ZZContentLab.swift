//
//  ZZContentLab.swift
//  BuildingWeather
//
//  Created by 陈钟 on 2019/5/6.
//  Copyright © 2019 陈钟. All rights reserved.
//

import UIKit

public class ZZContentLab: UIControl {

    public enum Alignment : NSInteger{
        case left,right,center,top,bottom
    }

    public enum TitleAlignment : NSInteger{
        case left,right,center
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override public init(frame: CGRect) {
        super.init(frame: frame)
        createViews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createViews()
    }
    public override func awakeFromNib() {
        super.awakeFromNib()
        createViews()
    }
    
    public var aligment : Alignment = .center{
        didSet{
            self.refreshView()
        }
    }
    public var titleAligment : TitleAlignment = .center{
        didSet{
            self.refreshView()
        }
    }
    public var conLabelAligmnt : TitleAlignment = .left{
        didSet{
            self.refreshView()
        }
    }
    
    public var titleConSpace : CGFloat = 0 {
        didSet{
            self.refreshView()
        }
    }
    public var leftRightSpace : CGFloat = 0{
        didSet{
            self.refreshView()
        }
    }
    public var leftRightTopOffset : CGFloat = 0{
        didSet{
            self.refreshView()
        }
    }

    public var contentOffset: CGPoint = .zero{
        didSet{
            self.refreshView()
        }
    }

    public var inset: UIEdgeInsets = .zero{
        didSet{
            self.refreshView()
        }
    }
    
    
    //MARK: - method
    public func refreshView() -> Void {
        var contentFrame = self.contentView.frame
        
        var titleSpace = self.titleConSpace
        if self.titleLabel.size.equalTo(CGSize.zero) {
            titleSpace = 0
        }
        let leftRight_Space = (self.leftConLabel.width == 0 || self.rightConLabel.width == 0) ? 0 : self.leftRightSpace
        
        contentFrame.zz_height = max(self.leftConLabel.height, self.rightConLabel.height) + self.titleLabel.height + titleSpace - self.inset.top - self.inset.bottom
        contentFrame.zz_width = max((self.leftConLabel.width + self.rightConLabel.width + leftRight_Space), self.titleLabel.width) - self.inset.left - self.inset.right
        
        switch self.titleAligment {
        case .left: do{
            self.titleLabel.origin = CGPoint.init(x: 0, y: 0)
            }
        case .right: do{
            self.titleLabel.origin = CGPoint.init(x: contentFrame.width - self.titleLabel.width, y: 0)
            }
        default: do{
            self.titleLabel.origin = CGPoint.init(x: (contentFrame.width - self.titleLabel.width) / 2, y: 0)
            }
        }
        switch self.conLabelAligmnt {
        case .left: do{
            self.leftConLabel.origin = CGPoint.init(x: 0, y: self.titleLabel.maxY + titleSpace)
            }
        case .right: do{
            self.leftConLabel.origin = CGPoint.init(x: contentFrame.width - leftRight_Space - self.leftConLabel.width - self.rightConLabel.width, y: self.titleLabel.maxY + titleSpace)
            }
        default: do{
            self.leftConLabel.origin = CGPoint.init(x: (contentFrame.width - leftRight_Space - self.leftConLabel.width - self.rightConLabel.width) / 2.0, y: self.titleLabel.maxY + titleSpace)
            }
        }
        self.rightConLabel.origin = CGPoint.init(x: self.leftConLabel.maxX + leftRight_Space, y: self.titleLabel.maxY + titleSpace)
        
        if leftRightTopOffset == 0 {
            if self.leftConLabel.height > self.rightConLabel.height{
                self.rightConLabel.y = self.leftConLabel.y + (self.leftConLabel.height - self.rightConLabel.height) / 2
            }else{
                self.leftConLabel.y = self.rightConLabel.y + (self.rightConLabel.height - self.leftConLabel.height) / 2
            }
        }else{
            if self.leftConLabel.height > self.rightConLabel.height{
                self.rightConLabel.y = self.leftConLabel.y + self.leftRightTopOffset
            }else{
                self.leftConLabel.y = self.rightConLabel.y + self.leftRightTopOffset
            }
        }
        
        
        contentFrame.y = (self.height - contentFrame.height) / 2
        contentFrame.x = (self.width - contentFrame.width) / 2
        switch self.aligment {
        case .left: do{
            contentFrame.x = self.inset.left;
            }
        case .right: do{
            contentFrame.x = self.width - contentFrame.width - self.inset.right
            }
        case .top: do{
            contentFrame.y = self.inset.top
            }
        case .bottom: do{
            contentFrame.y = self.height - contentFrame.height - self.inset.bottom
            }
        default: break
        }
        contentFrame.x += self.contentOffset.x
        contentFrame.y += self.contentOffset.y
        self.contentView.frame = contentFrame
    }
    
    //MARK: - views
    override public func layoutSubviews() {
        super.layoutSubviews()
        self.titleLabel.willChangedMaxWidth = self.width - self.contentOffset.x - self.inset.left - self.inset.right
//        self.titleLabel.text = self.titleLabel.text
        self.leftConLabel.willChangedMaxWidth = self.width - self.contentOffset.x - self.inset.left - self.inset.right
//        self.leftConLabel.text = self.leftConLabel.text
        self.rightConLabel.willChangedMaxWidth = self.width - self.leftConLabel.width - leftRightSpace - self.contentOffset.x - self.inset.left - self.inset.right
//        self.rightConLabel.text = self.rightConLabel.text

        self.titleLabel.refreshFrame()
        self.leftConLabel.refreshFrame()
        self.rightConLabel.refreshFrame()
    }
    public lazy var titleLabel: ZZUILabel = {
        let label = ZZUILabel.init()
        label.changeType = .All
        label.numberOfLines = 0
        label.willChangedMaxWidth = self.width
        label.textChangedBlock = {[weak self] label in
            self?.refreshView()
        }
        return label
    }()
    public lazy var leftConLabel: ZZUILabel = {
        let label = ZZUILabel.init()
        label.changeType = .All
        label.numberOfLines = 0
        label.willChangedMaxWidth = self.width
        label.textChangedBlock = {[weak self] label in
            self?.refreshView()
        }
        return label
    }()
    public lazy var rightConLabel: ZZUILabel = {
        let label = ZZUILabel.init()
        label.changeType = .All
        label.numberOfLines = 0
        label.willChangedMaxWidth = self.width
        label.textChangedBlock = {[weak self] label in
            self?.refreshView()
        }
        return label
    }()
    public lazy var contentView: UIView = {
        let contentView = UIView.init()
        contentView.isUserInteractionEnabled = false
        return contentView
    }()

    func createViews() -> Void {
        self.addSubview(self.contentView)
        self.contentView.addSubview(self.titleLabel)
        self.contentView.addSubview(self.leftConLabel)
        self.contentView.addSubview(self.rightConLabel)
    }

    open override var intrinsicContentSize: CGSize{
        return UIView.layoutFittingExpandedSize
    }
}
