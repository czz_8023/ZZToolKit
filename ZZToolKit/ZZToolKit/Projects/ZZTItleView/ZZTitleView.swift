//
//  ZZTitleView.swift
//  ZZTitleView
//
//  Created by 陈钟 on 2020/5/7.
//  Copyright © 2020 陈钟. All rights reserved.
//

import UIKit

open class ZZTitleView<T>: UIView where T: UIView {
    public enum ContentAlignment {
        case left,center,right
    }
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.createView()
    }

    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        self.createView()
    }
    /// 当item不能铺满整个View时进行对其  当能够整个View时 right 显示最后一个
    open var alignment: ContentAlignment = .center{
        didSet{
            self.refreshSelectedStyle(isClickBlock: false)
        }
    }
    /// 当item不能铺满整个View时进行对其  当能够整个View时 right 显示最后一个
    @discardableResult
    open func alignment(_ value: ContentAlignment) -> Self{
        self.alignment = value
        return self
    }

    /// 默认字体颜色
    open var font: UIFont = UIFont.systemFont(ofSize: UIFont.systemFontSize){
        didSet{
            self.refreshItemStyle()
            self.refreshSelectedStyle(isClickBlock: false)
        }
    }
    /// 默认字体颜色
    @discardableResult
    open func font(_ value: UIFont) -> Self{
        self.font = value
        return self
    }
    /// 选中字体颜色
    open var selectedFont: UIFont = UIFont.boldSystemFont(ofSize: UIFont.systemFontSize){
        didSet{
            self.refreshItemStyle()
            self.refreshSelectedStyle(isClickBlock: false)
        }
    }
    /// 选中字体颜色
    @discardableResult
    open func selectedFont(_ value: UIFont) -> Self{
        self.selectedFont = value
        return self
    }

    /// 默认字体颜色
    open var color: UIColor = UIColor.black{
        didSet{
            self.refreshItemStyle()
        }
    }
    /// 默认字体颜色
    @discardableResult
    open func color(_ value: UIColor) -> Self{
        self.color = value
        return self
    }
    /// 默认选中字体颜色
    open var selectedColor: UIColor = UIColor.systemBlue{
        didSet{
            self.refreshItemStyle()
        }
    }
    /// 默认选中字体颜色
    @discardableResult
    open func selectedColor(_ value: UIColor) -> Self{
        self.selectedColor = value
        return self
    }

    /// item之间的间隙
    open var itemSpace: CGFloat = 10{
        didSet{
            self.refreshSelectedStyle(isClickBlock: false)
        }
    }
    /// item之间的间隙
    @discardableResult
    open func itemSpace(_ value: CGFloat) -> Self{
        self.itemSpace = value
        return self
    }
    /// item宽高 增加量 width += itemMoreSize.width height += itemMoreSize.height
    open var itemMoreSize: CGSize = CGSize.zero{
        didSet{
            self.refreshSelectedStyle(isClickBlock: false)
        }
    }
    /// item宽高 增加量 width += itemMoreSize.width height += itemMoreSize.height
    @discardableResult
    open func itemMoreSize(_ value: CGSize) -> Self{
        self.itemMoreSize = value
        return self
    }
    /// item固定大小。 itemMoreSize失效
    open var itemFixedSize: CGSize = CGSize.zero{
        didSet{
            self.refreshSelectedStyle(isClickBlock: false)
        }
    }
    /// item固定大小。 itemMoreSize失效
    @discardableResult
    open func itemFixedSize(_ value: CGSize) -> Self{
        self.itemFixedSize = value
        return self
    }
    /// item偏移量
    open var itemOffset: CGPoint = CGPoint.zero{
        didSet{
            self.refreshSelectedStyle(isClickBlock: false)
        }
    }
    /// item偏移量
    @discardableResult
    open func itemOffset(_ value: CGPoint) -> Self{
        self.itemOffset = value
        return self
    }

    open var insets: UIEdgeInsets = UIEdgeInsets.zero{
        didSet{
            self.refreshSelectedStyle(isClickBlock: false)
        }
    }
    @discardableResult
    open func insets(_ value: UIEdgeInsets) -> Self{
        self.insets = value
        return self
    }

    private var _lightLineColor: UIColor? = nil
    /// 底部横线颜色 默认与选中字体颜色相同 当设置之后使用设置的颜色
    open var lightLineColor: UIColor{
        set{
            _lightLineColor = newValue
            self.refreshItemStyle()
        }
        get{
            return _lightLineColor ?? self.selectedColor
        }
    }
    /// 底部横线颜色 默认与选中字体颜色相同 当设置之后使用设置的颜色
    @discardableResult
    open func lightLineColor(_ value: UIColor) -> Self{
        self.lightLineColor = value
        return self
    }
    /// 底部横线 固定宽度 为0 时候自动跟随item宽度
    open var lightLintFixedWidth: CGFloat = 0{
        didSet{
            self.refreshSelectedStyle(isClickBlock: false)
        }
    }
    /// 底部横线 固定宽度 为0 时候自动跟随item宽度
    @discardableResult
    open func lightLineFixedWidth(_ value: CGFloat) -> Self{
        self.lightLintFixedWidth = value
        return self
    }
    /// 底部横线 固定高度
    open var lightLineFixedHeight: CGFloat = 4{
        didSet{
            self.lightLine.layer.cornerRadius = self.lightLineFixedHeight / 2.0
            self.refreshSelectedStyle(isClickBlock: false)
        }
    }
    /// 底部横线 固定高度
    @discardableResult
    open func lightLineFixedHeight(_ value: CGFloat) -> Self{
        self.lightLineFixedHeight = value
        return self
    }
    /// 地步横线的偏移量
    open var lightLineOffset: CGPoint = CGPoint.zero{
        didSet{
            self.refreshSelectedStyle(isClickBlock: false)
        }
    }
    /// 地步横线的偏移量
    @discardableResult
    open func lightLineOffset(_ value: CGPoint) -> Self{
        self.lightLineOffset = value
        return self
    }

    open var isRunToCenter: Bool = true{
        didSet{
            self.refreshSelectedStyle(isClickBlock: false)
        }
    }
    @discardableResult
    open func isRunToCenter(_ value: Bool) -> Self{
        self.isRunToCenter = value
        return self
    }
    
    /// 动画时间
    open var animationTime: TimeInterval = 0.25
    /// 动画时间
    @discardableResult
    open func animationTime(_ value: TimeInterval) -> Self{
        self.animationTime = value
        return self
    }
    
    open var isBackLightLine: Bool = false{
        didSet{
            if self.isBackLightLine{
                self.contentView.sendSubviewToBack(self.lightLine)
            }else{
                self.contentView.bringSubviewToFront(self.lightLine)
            }
        }
    }
    @discardableResult
    open func isBackLightLine(_ value: Bool) -> Self{
        self.isBackLightLine = value
        return self
    }
    open var isBackLightWithMoreSize: Bool = false{
        didSet{
            self.refreshSelectedStyle(isClickBlock: false)
        }
    }
    @discardableResult
    open func isBackLightWithMoreSize(_ value: Bool) -> Self{
        self.isBackLightWithMoreSize = value
        return self
    }

    private var _selectedIndex: Int = 0
    open var selectedIndex: Int{
        set{
            if newValue == _selectedIndex { return }
            _selectedIndex = newValue
            self.refreshSelectedStyle(isClickBlock: true)
        }
        get { return _selectedIndex }
//        didSet{
//            self.refreshSelectedStyle(isClickBlock: true)
//        }
    }
    @discardableResult
    open func selectedIndex(_ value: Int) -> Self{
        self.selectedIndex = value
        return self
    }
    private(set) var selectedItem: T?

    open var willRefreshStyleBlock: ((_ index: Int,_ item: T) -> ())?
    @discardableResult
    open func willRefreshStyleBlock(_ value: ((_ index: Int,_ item: T?) -> ())?) -> Self{
        self.willRefreshStyleBlock = value
        return self
    }
    open var didRefreshStyleBlock: ((_ index: Int,_ item: T) -> ())?
    @discardableResult
    open func didRefreshStyleBlock(_ value: ((_ index: Int,_ item: T?) -> ())?) -> Self{
        self.didRefreshStyleBlock = value
        return self
    }

    open var willSelectedItemBlock: ((_ index: Int,_ item: T?) -> ())?
    @discardableResult
    open func willSelectedItemBlock(_ value: ((_ index: Int,_ item: T?) -> ())?) -> Self{
        self.willSelectedItemBlock = value
        return self
    }
    open var didSelectedItemBlock: ((_ index: Int,_ item: T?) -> ())?
    @discardableResult
    open func didSelectedItemBlock(_ value: ((_ index: Int,_ item: T?) -> ())?) -> Self{
        self.didSelectedItemBlock = value
        return self
    }
    
    open var willClickedItemBlock: ((_ index: Int,_ item: T?) -> Bool)?
    @discardableResult
    open func willClickedItemBlock(_ value: ((_ index: Int,_ item: T?) -> Bool)?) -> Self{
        self.willClickedItemBlock = value
        return self
    }
    
    open var clickedItemBlock: ((_ index: Int,_ item: T?) -> ())?
    @discardableResult
    open func clickedItemBlock(_ value: ((_ index: Int,_ item: T?) -> ())?) -> Self{
        self.clickedItemBlock = value
        return self
    }

    private var isCreteateWithTitle: Bool = true
    private(set) var items: Array<T> = []
    /// 设置之后将绘制items cusItems失效
    open var titleArr: Array<String> = []{
        didSet{
            self.items.forEach({$0.removeFromSuperview()})
            self.items.removeAll()
//            self.cusItems.removeAll()
            self.isCreteateWithTitle = true
            self.selectedItem = nil

            self.titleArr.forEach { (_) in
                let item = T()
                if item is UIButton{
                    let butotn = item as! UIButton
                    butotn.addTarget(self, action: #selector(self.titleClick(button:)), for: .touchUpInside)
                }
                if item is ZZUIButton{
                    let butotn = item as! ZZUIButton
                    butotn.addTarget(self, action: #selector(self.titleClick(button:)), for: .touchUpInside)
                }
                self.contentView.addSubview(item)
                self.items.append(item)
            }
            self.refreshItemStyle()
            _selectedIndex = self.alignment == .right ? self.items.count - 1 : 0
        }
    }

    /// 设置之后将绘制items cusItems失效
    @discardableResult
    open func titleArr(_ value: Array<String>) -> Self{
        self.titleArr = value
        return self
    }

    /// 设置之后将绘制cusItems titleArr将失效
    open var cusItems: Array<T> = []{
        didSet{
            self.items.forEach({$0.removeFromSuperview()})
            self.items.removeAll()
//            self.titleArr.removeAll()
            self.isCreteateWithTitle = false
            self.selectedItem = nil

            self.cusItems.forEach { (item) in
                self.contentView.addSubview(item)
                self.items.append(item)
            }

            self.refreshItemStyle()
            _selectedIndex = self.alignment == .right ? self.items.count - 1 : 0
        }
    }

    /// 设置之后将绘制cusItems titleArr将失效
    @discardableResult
    open func cusItems(_ cusItems: Array<T>) -> Self{
        self.cusItems = cusItems
        return self
    }

    /// 找出item的Index。如果小于0则未找到
    @discardableResult
    open func index(with item: T) -> Int{
        return self.items.firstIndex(of: item) ?? -1
    }

    /// 找出index的item
    @discardableResult
    open func item(with index: Int) -> T?{
        if index < self.items.count {
            return self.items[index]
        }
        return nil
    }

    /// 选中某个View
    @discardableResult
    open func select(item: T) -> Self{
        let index = self.index(with: item)
        if index >= 0 {
            self.selectedIndex = index
        }
        return self
    }
    /// 选中某个View
    @discardableResult
    open func select(index: Int) -> Self{
        if index < self.items.count{
            self.selectedIndex = index
        }
        return self
    }

    @objc
    private func titleClick(button: UIControl){
        for index in 0 ..< self.items.count{
            let item = self.items[index]
            if item == button {
                if !(self.willClickedItemBlock?(index,item) ?? true) { return  }
                self.selectedIndex = index
                self.clickedItemBlock?(index, item)
            }
        }
    }

    private func textSize(_ text: String, font: UIFont? = nil, size:CGSize) -> CGSize {
        let pla = NSMutableParagraphStyle()
//        pla.lineSpacing = 1
        let titleAtt = NSMutableAttributedString(string: text)
        titleAtt.addAttribute(NSAttributedString.Key.paragraphStyle, value: pla, range: NSRange(location: 0, length: text.count))
        let label = UILabel.init(frame: CGRect.init(origin: CGPoint.zero, size: size))
        label.numberOfLines = 1
        if font != nil {
            label.font = font
        }
        label.attributedText = titleAtt
        label.sizeToFit()
        return label.frame.size
    }

    open func refreshItemFrame(isAnimation: Bool){
        if self.items.count == 0 { return }
        let contentViewFrame = self.bounds
        self.contentView.frame = contentViewFrame
        if self.isBackLightLine{
            self.contentView.sendSubviewToBack(self.lightLine)
        }else{
            self.contentView.bringSubviewToFront(self.lightLine)
        }

        var startX = self.insets.left
        for index in 0 ..< self.items.count{
            let item = self.items[index]
            var itemSize: CGSize = item.frame.size
            if self.itemFixedSize == CGSize.zero {
                if item is UIButton && self.isCreteateWithTitle{
                    let title  = self.titleArr[index]
                    let buttton = item as! UIButton
                    let font = (buttton == self.selectedItem) ? self.selectedFont : self.font
                    let textSize = self.textSize(title,
                                                 font: font,
                                                 size: CGSize(width:CGFloat(MAXFLOAT), height: self.frame.height))
                    itemSize = textSize + (buttton.imageView?.size ?? .zero)
                    itemSize.height = self.frame.height - self.insets.top - self.insets.bottom
                    itemSize.width += self.itemMoreSize.width
                    itemSize.height += self.itemMoreSize.height
                }
                if item is ZZUIButton && self.isCreteateWithTitle{
                    let title  = self.titleArr[index]
                    let buttton = item as! ZZUIButton
                    let font = (buttton == self.selectedItem) ? self.selectedFont : self.font
                    let textSize = self.textSize(title,
                                                 font: font,
                                                 size: CGSize(width:CGFloat(MAXFLOAT), height: self.frame.height))
                    itemSize = textSize + (buttton.imageView.size)
                    itemSize.height = self.frame.height - self.insets.top - self.insets.bottom
                    itemSize.width += self.itemMoreSize.width
                    itemSize.height += self.itemMoreSize.height
                }
            }else{
                itemSize = self.itemFixedSize
            }
//            itemSize.height = self.frame.height - self.insets.top - self.insets.bottom
//            var itemFrame = CGRect(origin: CGPoint(x: startX, y: self.insets.top), size: itemSize)
            var itemFrame = CGRect(origin: CGPoint(x: startX, y: self.insets.top + (self.frame.height - self.insets.top - self.insets.bottom - itemSize.height) / 2.0), size: itemSize)
            itemFrame.origin.x += self.itemOffset.x
            itemFrame.origin.y += self.itemOffset.y
            if isAnimation {
                UIView.animate(withDuration: animationTime, animations: {
                    item.frame = itemFrame
                })
            }else{
                item.frame = itemFrame
            }
            startX += (itemSize.width + self.itemSpace)
        }
        self.contentView.contentSize = CGSize(width: startX - self.itemSpace + self.insets.right, height: 0)
        if self.contentView.contentSize.width < contentViewFrame.width{
            switch self.alignment {
                case .center:
                    let inset: CGFloat = (self.frame.width - self.contentView.contentSize.width) / 2.0
                    self.contentView.contentInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: 0)
                    break
                case .right:
                    let inset: CGFloat = (self.frame.width - self.contentView.contentSize.width)
                    self.contentView.contentInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: 0)
                    break
                default:
                    self.contentView.contentInset = UIEdgeInsets.zero
                    break
            }
        }else{
            self.contentView.contentInset = UIEdgeInsets.zero
        }

    }

    open func refreshItemStyle(){
        if self.items.count == 0 { return }
        for index in 0 ..< self.items.count{
            let item = self.items[index]
            self.willRefreshStyleBlock?(index, item)
            if item is UIButton && self.isCreteateWithTitle{
                let buttton = item as! UIButton
                let title  = self.titleArr[index]
                buttton.setTitle(title, for: .normal)
                buttton.setTitleColor(self.color, for: .normal)
                buttton.setTitleColor(self.selectedColor, for: .selected)
                buttton.titleLabel?.font = self.font
            }
            if item is ZZUIButton && self.isCreteateWithTitle{
                let buttton = item as! ZZUIButton
                let title  = self.titleArr[index]
                buttton.title(title: title, state: .normal)
                buttton.titleColor(color: self.color, state: .normal)
                buttton.titleColor(color: self.selectedColor, state: .selected)
                buttton.titleLabel({$0.font = self.font})
            }
            self.didRefreshStyleBlock?(index, item)
        }
        self.refreshSelectedStyle(isClickBlock: false)
    }

    private var animationLabels: [UILabel: UILabel] = [:]
    private func animate(color: UIColor, by label: UILabel){
        if let l = animationLabels[label] {
            l.removeFromSuperview()
            animationLabels.removeValue(forKey: label)
        }
        
        let animateLabel = UILabel()
        animateLabel.text = label.text
        animateLabel.font = label.font
        animateLabel.textColor = color
        label.addSubview(animateLabel)
        animateLabel.sizeToFit()
        animationLabels[label] = animateLabel
        UIView.animate(withDuration: animationTime, delay: 0, options: .curveEaseInOut) {
            animateLabel.alpha = 0
        } completion: { [weak self] (_) in
            if let l = self?.animationLabels[label] {
                l.removeFromSuperview()
                self?.animationLabels.removeValue(forKey: label)
            }
        }
    }
    
    open func refreshSelectedStyle(isClickBlock: Bool){
        if self.items.count == 0 { return }
        if self.items.count <= self.selectedIndex { return }
        
        if self.selectedItem is UIButton && self.isCreteateWithTitle{
            if let buttton = self.selectedItem as? UIButton{
                buttton.titleLabel?.font = self.font
                buttton.isSelected = false
                if let titleLabel = buttton.titleLabel, isClickBlock{
                    let scaled = self.selectedFont.pointSize / self.font.pointSize
                    self.animate(color: self.selectedColor, by: titleLabel)
                    titleLabel.transform = titleLabel.transform.scaledBy(x: scaled, y: scaled)
                    UIView.animate(withDuration: animationTime, delay: 0, options: .curveEaseInOut) {
                        titleLabel.transform = .identity
                    } completion: { (_) in
                    }
                }
            }
        }
        if self.selectedItem is ZZUIButton && self.isCreteateWithTitle{
            if let buttton = self.selectedItem as? ZZUIButton{
                buttton.titleLabel({$0.font = self.font})
                buttton.isSelected = false
                if isClickBlock{
                    let titleLabel = buttton.titleLabel
                    let scaled = self.selectedFont.pointSize / self.font.pointSize
                    self.animate(color: self.selectedColor, by: titleLabel)
                    titleLabel.transform = titleLabel.transform.scaledBy(x: scaled, y: scaled)
                    UIView.animate(withDuration: animationTime, delay: 0, options: .curveEaseInOut) {
                        titleLabel.transform = .identity
                    } completion: { (_) in
                        
                    }
                }
            }
        }
        isClickBlock ? self.willSelectedItemBlock?(self.selectedIndex,self.selectedItem) : nil

        self.selectedItem = self.items[self.selectedIndex]
        if self.selectedItem is UIButton && self.isCreteateWithTitle{
            if let buttton = self.selectedItem as? UIButton{
                buttton.titleLabel?.font = self.selectedFont
                buttton.isSelected = true
                if let titleLabel = buttton.titleLabel, isClickBlock{
                    let scaled = self.font.pointSize / self.selectedFont.pointSize
                    self.animate(color: self.color, by: titleLabel)
                    titleLabel.transform = titleLabel.transform.scaledBy(x: scaled, y: scaled)
                    UIView.animate(withDuration: animationTime, delay: 0, options: .curveEaseInOut) {
                        titleLabel.transform = .identity
                    } completion: { (_) in
                        
                    }
                }
            }
        }
        if self.selectedItem is ZZUIButton && self.isCreteateWithTitle{
            if let buttton = self.selectedItem as? ZZUIButton{
                buttton.titleLabel({$0.font = self.selectedFont})
                buttton.isSelected = true
                if isClickBlock{
                    let titleLabel = buttton.titleLabel
                    let scaled = self.font.pointSize / self.selectedFont.pointSize
                    self.animate(color: self.color, by: titleLabel)
                    titleLabel.transform = titleLabel.transform.scaledBy(x: scaled, y: scaled)
                    UIView.animate(withDuration: animationTime, delay: 0, options: .curveEaseInOut) {
                        titleLabel.transform = .identity
                    } completion: { (_) in
                        
                    }
                }
            }
        }
        isClickBlock ? self.didSelectedItemBlock?(self.selectedIndex,self.selectedItem) : nil


        self.refreshItemFrame(isAnimation: isClickBlock)


        self.lightLine.backgroundColor = self.lightLineColor
        var lightLineFrame = self.selectedItem?.frame ?? CGRect.zero
        lightLineFrame.origin.y = self.contentView.frame.height - self.lightLineFixedHeight - self.insets.bottom
        lightLineFrame.size.height = self.lightLineFixedHeight
        lightLineFrame.origin.x += self.lightLineOffset.x
        lightLineFrame.origin.y += self.lightLineOffset.y
        if self.selectedItem is UIButton && self.isCreteateWithTitle{
            let buttton = self.selectedItem as? UIButton
            let size = self.textSize(buttton?.titleLabel?.text ?? "",
                                     font: buttton?.titleLabel?.font,
                                     size: CGSize(width:CGFloat(MAXFLOAT), height: self.frame.height))
            lightLineFrame.size.width = size.width + (buttton?.imageView?.size.width ?? 0) +  (self.isBackLightWithMoreSize ? self.itemMoreSize.width : 0)
        }
        if self.selectedItem is ZZUIButton && self.isCreteateWithTitle{
            let buttton = self.selectedItem as? ZZUIButton
            lightLineFrame.size.width = (buttton?.contentView.width ?? 0) + (self.isBackLightWithMoreSize ? self.itemMoreSize.width : 0)
        }
        if self.lightLintFixedWidth != 0 {
            lightLineFrame.size.width = self.lightLintFixedWidth
        }
        lightLineFrame.origin.x += ((self.selectedItem?.frame.size.width ?? 0) - lightLineFrame.size.width) / 2.0

        if isClickBlock == true {
            UIView.animate(withDuration: animationTime, animations: { [weak self] in
                guard let `self` = self else { return }
                self.refreshLine(frame: lightLineFrame)
            })
        }else{
            self.refreshLine(frame: lightLineFrame)
        }
    }
    
    private func refreshLine(frame lightLineFrame: CGRect){
        self.lightLine.frame = lightLineFrame

        if self.isRunToCenter {
            if self.contentView.contentInset.left != 0 { return }
            if self.contentView.contentSize.width < self.contentView.frame.width { return }
            let selectedItemFrame = self.selectedItem?.frame ?? CGRect.zero
            let wid = self.frame.width / 2.0
            let selectItemCenterX = selectedItemFrame.origin.x + selectedItemFrame.width / 2.0
            if selectItemCenterX > wid && selectItemCenterX < self.contentView.contentSize.width - wid {
                self.contentView.contentOffset = CGPoint(x: selectItemCenterX - wid, y: 0)
            }else if (selectItemCenterX < wid){
                self.contentView.contentOffset = .zero
            }else if (selectItemCenterX > self.contentView.contentSize.width - wid){
                self.contentView.contentOffset = CGPoint(x: self.contentView.contentSize.width - self.frame.width, y: 0)
            }
        }
    }

    private func createView(){
        self.addSubview(self.contentView)
        self.contentView.addSubview(self.lightLine)
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        if self.isBackLightLine{
            self.contentView.sendSubviewToBack(self.lightLine)
        }else{
            self.contentView.bringSubviewToFront(self.lightLine)
        }
        self.refreshSelectedStyle(isClickBlock: false)
    }


    lazy open var contentView: UIScrollView = {
        let view = UIScrollView()
        if #available(iOS 13.0, *) {
            view.contentInsetAdjustmentBehavior = .never
        }
        view.alwaysBounceHorizontal = true
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        return view
    }()

    @discardableResult
    open func contentView(_ view:((_ contentView: UIScrollView) -> ())) -> Self{
        view(self.contentView)
        return self
    }

    lazy open var lightLine: UIImageView = {
        let view = UIImageView()
        view.layer.cornerRadius = self.lightLineFixedHeight / 2.0
        view.layer.masksToBounds = true
        return view
    }()

    @discardableResult
    open func lightLine(_ view:((_ lightLine: UIImageView) -> ())) -> Self{
        view(self.lightLine)
        return self
    }

}
