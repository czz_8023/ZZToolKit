//
//  Device+ZZ.swift
//  ZZToolKit
//
//  Created by 陈钟 on 2020/2/21.
//  Copyright © 2020 陈钟. All rights reserved.
//

import Foundation
import UIKit

public extension UIDevice{
    static var isSimuLator: Bool{
        return (TARGET_IPHONE_SIMULATOR == 1 && TARGET_OS_IPHONE == 1)
    }
}
