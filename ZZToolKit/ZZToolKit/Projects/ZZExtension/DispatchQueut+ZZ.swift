//
//  DispatchQueut+ZZ.swift
//  TianYiXing
//
//  Created by 陈钟 on 2019/8/12.
//  Copyright © 2019 陈钟. All rights reserved.
//

import Foundation
public extension DispatchQueue{
    private static var _onceToken: [String] = []
    class func once(token: String = "\(#file):\(#function):\(#line)", block: ()->Void) {
        objc_sync_enter(self)
        defer
        {
            objc_sync_exit(self)
        }
        if _onceToken.contains(token)
        {
            return
        }
        _onceToken.append(token)
        block()
    }
}
