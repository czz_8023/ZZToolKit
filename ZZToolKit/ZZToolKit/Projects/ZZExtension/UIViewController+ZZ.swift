//
//  UIViewController+ZZ.swift
//  KeDaLeDaoHang
//
//  Created by 陈钟 on 2019/5/27.
//  Copyright © 2019 陈钟. All rights reserved.
//

import Foundation
import UIKit

public extension UIViewController{
    
    func push(viewCtrl:UIViewController,objct:Any? = nil) -> Void {
        if let obj = objct {
            viewCtrl.object = obj
        }
        viewCtrl.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(viewCtrl, animated: true)
    }
    
    func pop(afterTime: Double = 0, animation: Bool = true) -> Void {
        if afterTime == 0 {
            self.navigationController?.popViewController(animated: animation)
            return
        }
        self.zz_backButton()?.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + afterTime) { [weak self] in
            self?.zz_backButton()?.isEnabled = true
            self?.navigationController?.popViewController(animated: animation)
        }
    }
    
    /// ZZUINavigationCtrl 跳转界面的返回按钮
    func zz_backButton() -> UIButton? {
        return self.navigationItem.leftBarButtonItem?.customView as? UIButton
    }

    var zzPushContentViewCtrl: ZZPushContentViewCtrl?{
        return self.navigationController?.parent as? ZZPushContentViewCtrl
    }

    var zzNavigationController: ZZUINavigationCtrl?{
        return self.zzPushContentViewCtrl?.navigationController as? ZZUINavigationCtrl
    }
}
