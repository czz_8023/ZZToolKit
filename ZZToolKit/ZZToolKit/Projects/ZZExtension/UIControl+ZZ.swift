//
//  UIControl+ZZ.swift
//  ZZToolKit
//
//  Created by 陈钟 on 2019/12/20.
//  Copyright © 2019 陈钟. All rights reserved.
//

import Foundation
import UIKit

open class UIContrlBlockTarget: NSObject {
    typealias TargetAlias = (_ sender: Any) -> Void
    var block: TargetAlias?
    var events: UIControl.Event = []

    init(block: @escaping TargetAlias, events: UIControl.Event) {
        super.init()
        self.block = block
        self.events = events
    }

    @objc
    func invoke(sender: Any){
        self.block?(sender)
    }
}

public extension UIControl{

    var targets: [UIContrlBlockTarget]{
        set{
            let key: UnsafeRawPointer! = UnsafeRawPointer.init(bitPattern: "targets".hashValue)
            objc_setAssociatedObject(self, key, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        get{
            let key: UnsafeRawPointer! = UnsafeRawPointer.init(bitPattern: "targets".hashValue)
            let obj = objc_getAssociatedObject(self, key)
            return (obj as? [UIContrlBlockTarget]) ?? []
        }
    }

    /// 添加事件
    @discardableResult
    func addBlock(for events:UIControl.Event, block:@escaping ((_ sender: Any) -> Void)) -> Self{
        let blockTages = UIContrlBlockTarget.init(block: block, events: events)
        self.addTarget(blockTages, action: #selector(blockTages.invoke(sender:)), for: events)
        self.targets.append(blockTages)
        return self
    }

    @discardableResult
    func addTarget(for controlEvents: UIControl.Event,target: Any?, action: Selector) -> Self{
        self.addTarget(target, action: action, for: controlEvents)
        return self
    }

    /// 删除通过block绑定的事件
    @discardableResult
    func removeBlockTarget(for events: UIControl.Event) -> Self{
        let eventTargets = self.targets.filter({ $0.events == events })
        eventTargets.forEach { (target) in
            self.removeTarget(target, action: #selector(target.invoke(sender:)), for: target.events)
        }
        self.targets.removeAll(where: { $0.events == events })
        return self
    }

    /// 删除所有的点击事件
    @discardableResult
    func removeAllTarget() -> Self{
        self.allTargets.forEach { (obj) in
            self.removeTarget(obj, action: nil, for: UIControl.Event.allEvents)
        }
        self.targets.removeAll()
        return self
    }


    @discardableResult
    @objc dynamic func isEnabled(_ enable: Bool) -> Self{
        self.isEnabled = enable
        return self
    }
    
    @discardableResult
    @objc dynamic func isSelected(_ selected: Bool) -> Self{
        self.isSelected = selected
        return self
    }

}
