//
//  CGPoint+ZZ.swift
//  TianYiXing
//
//  Created by 陈钟 on 2019/7/9.
//  Copyright © 2019 陈钟. All rights reserved.
//

import Foundation
import UIKit

public extension CGPoint{
    init(point:CGPoint) {
        self.init()
        self.x = point.x
        self.y = point.y
    }

    static func +(left: CGPoint, right: CGPoint) -> CGPoint{
        return CGPoint(x: left.x + right.x, y: left.y + right.y)
    }

    static func -(left: CGPoint, right: CGPoint) -> CGPoint{
        return CGPoint(x: left.x - right.x, y: left.y - right.y)
    }
}



public extension CGSize{
    init(_ value: CGFloat) {
        self.init()
        self.width = value
        self.height = value
    }

    init(_ width: CGFloat, _ height: CGFloat) {
        self.init()
        self.width = width
        self.height = height
    }

    static func += (left:inout CGSize, right: CGSize){
        left.width += right.width
        left.height += right.height
    }

    static func -= (left:inout CGSize, right: CGSize){
        left.width -= right.width
        left.height -= right.height
    }

    static func + (left:CGSize, right: CGSize) -> CGSize{
        var size: CGSize = left
        size.width += right.width
        size.height += right.height
        return size
    }

    static func - (left:CGSize, right: CGSize) -> CGSize{
        var size: CGSize = left
        size.width -= right.width
        size.height -= right.height
        return size
    }
}
