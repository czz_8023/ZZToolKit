//
//  NSArray+ZZ.swift
//  KeDaLeDaoHang
//
//  Created by 陈钟 on 2019/6/3.
//  Copyright © 2019 陈钟. All rights reserved.
//

import Foundation

public extension Array {
    /// 防止数组越界
    subscript(index: Int, safe: Bool) -> Element? {
        if safe {
            if index < 0 { return nil}
            if self.count > index {
                return self[index]
            }
            else {
                return nil
            }
        }
        else {
            return self[index]
        }
    }
    
    func objAt(index:Int) ->  Element? {
        return self[index,true]
    }
    
    /// 获取数组中元素的 某一个属性值
    func subValueArr(keyValye:((_ element:Element) -> String?)) -> [String] {
        var arr: [String] = []
        for index in 0 ..< self.count {
            if let subValue = self.objAt(index: index){
                if let e = keyValye(subValue){
                    arr.append(e)
                }
            }
        }
        return arr
    }
    
}
