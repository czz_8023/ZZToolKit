//
//  CALayer+ZZ.swift
//  ZZToolKit
//
//  Created by 陈钟 on 2019/12/27.
//  Copyright © 2019 陈钟. All rights reserved.
//

import Foundation
import UIKit

public extension CALayer{
    /// 删除所有的子Layer
    func removeAllSubLayers(){
        while (self.sublayers?.count ?? 0) > 0 {
            self.sublayers?.last?.removeFromSuperlayer()
        }
    }

    var size: CGSize{
        set{
            var frame = self.frame
            frame.size = newValue
            self.frame = frame
        }
        get{
            return self.frame.size
        }
    }

    var center: CGPoint{
        set{
            var frame = self.frame
            frame.origin.x = newValue.x - frame.size.width * 0.5
            frame.origin.y = newValue.y - frame.size.height * 0.5
            self.frame = frame
        }
        get{
            return CGPoint(x: self.frame.x + self.frame.width * 0.5,
                           y: self.frame.y + self.frame.height + 0.5)
        }
    }
}

