//
//  Dictionary+ZZ.swift
//  KeDaLeDaoHang
//
//  Created by 陈钟 on 2019/5/31.
//  Copyright © 2019 陈钟. All rights reserved.
//

import Foundation

public extension Dictionary{
    mutating func append(dic:Dictionary) -> Void {
        for (dicKey,dicValue) in dic {
            self[dicKey] = dicValue
        }
    }
}
