//
//  UIViewCtroller+ZZ.swift
//  TestChuanShanJia
//
//  Created by CZZ on 2020/5/15.
//  Copyright © 2020 陈钟. All rights reserved.
//

import Foundation
import UIKit

public struct ZZBase<Base> {
    public let base: Base
    public init(_ base: Base) {
        self.base = base
    }
}

public protocol ZZBaseModel: AnyObject {}

public extension ZZBaseModel{
    var zz: ZZBase<Self>{
        get { return ZZBase(self)}
        set {}
    }
}

extension UIViewController: ZZBaseModel {}

extension ZZBase where Base: UIViewController{

    public var naviBarBgImage: UIImage? {
        get {
            if #available(iOS 15.0, *) {
                if let standardAppearance = self.base.navigationController?.navigationBar.standardAppearance{
                    return standardAppearance.backgroundImage
                }
            }
            return self.base.navigationController?.navigationBar.backgroundImage(for: UIBarMetrics.default)
        }
        set {
            self.base.navigationController?.navigationBar.setBackgroundImage(newValue, for: UIBarMetrics.default)
            if #available(iOS 15.0, *) {
                if let standardAppearance = self.base.navigationController?.navigationBar.standardAppearance.copy(){
                    standardAppearance.backgroundImage = newValue
                    self.base.navigationController?.navigationBar.standardAppearance = standardAppearance
                    self.base.navigationController?.navigationBar.scrollEdgeAppearance = standardAppearance
                }
            }
        }
    }

    public var shadowImag: UIImage?{
        get{
            if #available(iOS 15.0, *) {
                if let standardAppearance = self.base.navigationController?.navigationBar.standardAppearance{
                    return standardAppearance.shadowImage
                }
            }
            return self.base.navigationController?.navigationBar.shadowImage
        }
        set {
            self.base.navigationController?.navigationBar.shadowImage = newValue
            if #available(iOS 15.0, *) {
                if let standardAppearance = self.base.navigationController?.navigationBar.standardAppearance.copy(){
                    standardAppearance.shadowImage = newValue
                    self.base.navigationController?.navigationBar.standardAppearance = standardAppearance
                    self.base.navigationController?.navigationBar.scrollEdgeAppearance = standardAppearance
                }
            }
        }
    }

    public var titleColor: UIColor?{
        get {
            if #available(iOS 15.0, *) {
                if let standardAppearance = self.base.navigationController?.navigationBar.standardAppearance{
                    return standardAppearance.titleTextAttributes[NSAttributedString.Key.foregroundColor] as? UIColor
                }
            }
            return self.base.navigationController?.navigationBar.titleTextAttributes?[NSAttributedString.Key.foregroundColor] as? UIColor
        }
        set{
            var titleAtt = self.base.navigationController?.navigationBar.titleTextAttributes ?? [:]
            if let color = newValue {
                titleAtt[NSAttributedString.Key.foregroundColor] = color
                self.base.navigationController?.navigationBar.titleTextAttributes = titleAtt
                if #available(iOS 15.0, *) {
                    if let standardAppearance = self.base.navigationController?.navigationBar.standardAppearance.copy(){
                        standardAppearance.titleTextAttributes = titleAtt
                        self.base.navigationController?.navigationBar.standardAppearance = standardAppearance
                        self.base.navigationController?.navigationBar.scrollEdgeAppearance = standardAppearance
                    }
                }
            }else{
                self.base.navigationController?.navigationBar.titleTextAttributes?.removeValue(forKey: NSAttributedString.Key.foregroundColor)
            }
        }
    }

    public var titleFont: UIFont?{
        get {
            if #available(iOS 15.0, *) {
                if let standardAppearance = self.base.navigationController?.navigationBar.standardAppearance{
                    return standardAppearance.titleTextAttributes[NSAttributedString.Key.font] as? UIFont
                }
            }
            return self.base.navigationController?.navigationBar.titleTextAttributes?[NSAttributedString.Key.font] as? UIFont
        }
        set{
            var titleAtt = self.base.navigationController?.navigationBar.titleTextAttributes ?? [:]
            if let color = newValue {
                titleAtt[NSAttributedString.Key.font] = color
                self.base.navigationController?.navigationBar.titleTextAttributes = titleAtt
                if #available(iOS 15.0, *) {
                    if let standardAppearance = self.base.navigationController?.navigationBar.standardAppearance.copy(){
                        standardAppearance.titleTextAttributes = titleAtt
                        self.base.navigationController?.navigationBar.standardAppearance = standardAppearance
                        self.base.navigationController?.navigationBar.scrollEdgeAppearance = standardAppearance
                    }
                }
            }else{
                self.base.navigationController?.navigationBar.titleTextAttributes?.removeValue(forKey: NSAttributedString.Key.font)
            }
        }
    }
}
