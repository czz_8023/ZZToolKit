//
//  UINavigation+ZZ.swift
//  KeDaLeDaoHang
//
//  Created by 陈钟 on 2019/6/17.
//  Copyright © 2019 陈钟. All rights reserved.
//

import Foundation
import UIKit

public extension UINavigationController{
    @discardableResult @objc
    func popToClass(vcClass:AnyClass, animated: Bool) -> [UIViewController]? {
        var willPushCtrl:UIViewController? = nil
        for viewCtrl in self.viewControllers {
            if viewCtrl.isKind(of: vcClass) {
                willPushCtrl = viewCtrl
                break
            }
        }
        if  willPushCtrl == nil {
            self.popViewController(animated: animated)
            return nil
        }
        return self.popToViewController(willPushCtrl!, animated: animated)
    }
}
