//
//  ZZToolOC.swift
//  ZZToolKit
//
//  Created by 123 on 2020/12/30.
//  Copyright © 2020 陈钟. All rights reserved.
//

import Foundation
import UIKit

extension ZZTool{
    
    /// 颜色
    @objc public static func kRGB(r:CGFloat,g:CGFloat,b:CGFloat) -> UIColor {
        return RGB(r: r, g: g, b: b)
    }
    /// 颜色
    @objc public static func kRGB(r:CGFloat,g:CGFloat,b:CGFloat,a:CGFloat) -> UIColor {
        return RGB(r: r, g: g, b: b, a: a)
    }

    @available(iOS 8.2, *)
    @objc public static func kFontSize(size:CGFloat, weight: UIFont.Weight) -> UIFont{
        return FontSize(size: size, weight: weight)
    }
    
//    @available(iOS, introduced: 6.0, deprecated: 8.2)
    @objc public static func kFontSize(size:CGFloat) -> UIFont{
        return FontSize(size: size)
    }
    
    @objc public static func kFontBoldSize(size:CGFloat) -> UIFont {
        return FontBoldSize(size: size)
    }

    /// UUID
    @objc public static func kUuidString() -> String{
        return UIDevice.current.identifierForVendor?.uuidString ?? ""
    }

    /// Keywindow
    @objc public static func kKeyWindow() -> UIWindow? {
        return keyWindow
    }

    /// 用户手机别名
    @objc public static func kPhoneName() -> String {
        return PhoneName
    }

    /// 设备名称
    @objc public static func kSystemName() -> String {
        return SystemName
    }

    /// 设备系统版本
    @objc public static func kSystemVersion() -> String {
        return SystemVersion
    }

    /// APP版本
    @objc public static func kAppVersion() -> String {
        return appVersion
    }

    /// APP Build
    @objc public static func kAppBuild() -> String {
        return appBuild
    }

    /// APP名字
    @objc public static func kAppName() -> String {
        return appName
    }

    /// 是否是iPhone X
    @objc public static func kIsIPhoneX() -> Bool {
        return isIPhoneX
    }

    /// 是否是 iphone5 及以下
    @objc public static func kIsIphone5OrLess() -> Bool {
        return isIphone5OrLess
    }

    /// 是否是iPad
    @objc public static func kIsIpad() -> Bool {
        return isIpad
    }

    /// 是否是iPhone
    @objc public static func kIsIphone() -> Bool {
        return isIphone
    }

    /// 是否是暗黑模式
    @objc public static func kIsDarkModel() -> Bool{
       return isDarkModel
    }

    /// 是否是Retina
    @objc public static func kIsRetina() -> Bool {
        return isRetina
    }

    /// 屏幕宽度
    @objc public static func kScreen_width() -> CGFloat {
        return screen_width
    }

    /// 屏幕高度
    @objc public static func kScreen_height() -> CGFloat {
        return screen_height
    }

    /// 屏幕最大边
    @objc public static func kScreen_max_length() -> CGFloat {
        return screen_max_length
    }

    /// 屏幕最小边
    @objc public static func kScreen_min_length() -> CGFloat {
        return screen_min_length
    }

    /// 最小边的比例
    @objc public static func kScreen_min_scale() -> CGFloat {
        return screen_min_scale
    }

    /// 状态栏高度
    @objc public static func kStatusBarHeight() -> CGFloat {
        return statusBarHeight
    }

    /// navigationBar 高度
    @objc public static func kNavigationBarHeight() -> CGFloat{
        return navigationBarHeight
    }

    /// iphonex 下方多出高度
    @objc public static func kSafeAreaHeight() -> CGFloat {
        return safeAreaHeight
    }

    /// TabBar 高度
    @objc public static func kTabBarHeight() -> CGFloat {
        return tabBarHeight
    }


    //MARK: - 文件路径

    /// bundle
    @objc public static func kFile_bundle(name: String,type: String) -> String? {
        return file_bundle(name: name, type: type)
    }

    /// documents
    @objc public static func kFile_documents() -> String {
        ZZLog("")
        return file_documents()
    }

    /// documents filename
    @objc public static func kFile_documents(filename: String) -> String {
        return file_documents(filename: filename)
    }

    /// 将字符串转换成URL
    @objc public static func kUrl(url string:String?) -> URL? {
        return Url(url: string)
    }
    /// 字符串转 文件路径URL
    @objc public static func kUrl(file string:String?) -> URL? {
        return Url(file: string)
    }
}
