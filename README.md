# ZZToolKit

#### 介绍
ZZToolKit是一个自定义工具库，其中包含各种常用类的属性扩展及一些自定义组件。

### ZZTool类<br> 
包含各种基础属性获取和方法
    
    /// 颜色<br>
    public func RGB(r:CGFloat,g:CGFloat,b:CGFloat,a:CGFloat = 1) -> UIColor
    
    @available(iOS 8.2, *)<br>
    public func FontSize(size:CGFloat, weight: UIFont.Weight) -> UIFont
    
    //@available(iOS, introduced: 6.0, deprecated: 8.2)<br>
    public func FontSize(size:CGFloat) -> UIFont
    
    public func FontBoldSize(size:CGFloat) -> UIFont
    
    /// UUID<br>
    public var uuidString: String
    
    /// Keywindow<br>
    public var keyWindow: UIWindow?
    
    /// 用户手机别名<br>
    public var PhoneName: String 
    
    /// 设备名称<br>
    public var SystemName: String 
    
    /// 设备系统版本<br>
    public var SystemVersion: String
    
    /// APP版本<br>
    public var appVersion: String 
    
    /// APP Build<br>
    public var appBuild: String 
    
    /// APP名字<br>
    public var appName: String
    
    /// 是否是iPhone X<br>
    public var isIPhoneX: Bool
    
    /// 是否是 iphone5 及以下<br>
    public var isIphone5OrLess: Bool 
    
    /// 是否是iPad<br>
    public var isIpad: Bool 
    
    /// 是否是iPhone<br>
    public var isIphone: Bool 
    
    /// 是否是暗黑模式<br>
    public var isDarkModel: Bool
    
    /// 是否是Retina<br>
    public var isRetina: Bool 
    
    /// 屏幕宽度<br>
    public var screen_width: CGFloat 
    
    /// 屏幕高度<br>
    public var screen_height: CGFloat 
    
    /// 屏幕最大边<br>
    public var screen_max_length: CGFloat 
    
    /// 屏幕最小边<br>
    public var screen_min_length: CGFloat 
    
    /// 最小边的比例<br>
    public var screen_min_scale: CGFloat 
    
    /// 状态栏高度<br>
    public var statusBarHeight: CGFloat 
    
    /// navigationBar 高度<br>
    public let navigationBarHeight: CGFloat
    
    /// iphonex 下方多出高度<br>
    public let safeAreaHeight: CGFloat 
    
    /// TabBar 高度<br>
    public let tabBarHeight: CGFloat 
    
    /// 文件路径<br>
    public func file_bundle(name: String,type: String) -> String? 
    
    /// documents<br>
    public func file_documents() -> String 
    
    /// documents filename<br>
    public func file_documents(filename: String) -> String
    
    /// 将字符串转换成URL<br>
    public func Url(url string:String?) -> URL? 
    
    /// 字符串转 文件路径URL<br>
    public func Url(file string:String?) -> URL?
    
    //MARK: - LOG<br>
    public func ZZLog<T>(_ message:T,file:String = #file,funcName:String = #function,lineNum:Int = #line)

### 属性扩展类
#### 1、UIWindow<br>

    func visibleViewController() -> UIViewController?

    static func getVisibleViewController(from viewCtrl: UIViewController) -> UIViewController

    /// 获取当前显示窗体
    static func getCurrentViewController() -> UIViewController?

#### 2、UIViewController<br>

    public var naviBarBgImage: UIImage? 

    public var shadowImag: UIImage?

    public var titleColor: UIColor?

    public var titleFont: UIFont?

#### 3、UIScrollView 实现老板支付宝iOS客户端首页拖动的效果，<br>

    /// 设置顶部显示的窗体 将在设置zz_headerViewHeight后自动调整高度 宽度将自动调整
    var zz_topView: UIView?

    /// 顶部窗体的显示高度
    var zz_topViewHeight: CGFloat

    /// 顶部窗体的显示宽度
    var zz_topViewWidth: CGFloat

    /// 窗体拖出显示区域后 固定显示高度（高度由窗体底部向上计算）
    var zz_topViewShowHeight: CGFloat

    /// 是否向下拖动的时候 topview一直处于顶部
    var isBindingTop: Bool 

#### 4、UIAlertController

    @discardableResult static func show(actions:[UIAlertAction],
                      viewCtrl:UIViewController,
                      view:UIView?,
                      title:String?,
                      message:String?,
                      style:UIAlertController.Style) -> UIAlertController

    /// 显示一个系统Alert弹窗
    /// - Parameters:
    ///   - alert: 自定义UIAlertAction对象数组
    ///   - viewCtrl: presentViewController
    ///   - view: 显示位置
    ///   - title: 标题
    ///   - message: 内容
    static func show(alert:[UIAlertAction],
                     viewCtrl:UIViewController,
                     view:UIView?,
                     title:String?,
                     message:String?)

    /// 显示一个系统Alert弹窗
    /// - Parameters:
    ///   - title: 标题
    ///   - message: 内容
    ///   - okTitle: 确定标题
    ///   - handler: 取消标题
    static func show(alert title: String?, message: String?, okTitle: String?, handler: ((UIAlertAction) -> Void)? = nil)

    /// 展示一个系统ActionSheet
    /// - Parameters:
    ///   - actionSheet: UIAlertAction对象数组
    ///   - viewCtrl: presentViewController
    ///   - view: 显示位置
    ///   - title: 标题
    ///   - message: 内容
    static func show(actionSheet:[UIAlertAction],
                     viewCtrl:UIViewController,
                     view:UIView?,
                     title:String?,
                     message:String?)

    /// 展示一个系统弹窗
    /// - Parameters:
    ///   - titles: 按钮titles对象数据
    ///   - viewCtrl: presentViewController
    ///   - view: 显示位置
    ///   - title: 标题
    ///   - message: 内容
    ///   - style: 样式
    ///   - titleBlock: titles回调 自行处理 titles中对象需要内显示的title
    ///   - clickBlock: 选项按钮点击调用
    static func showT<T>(titles:[T],
                         viewCtrl:UIViewController,
                         view:UIView?,
                         title:String?,
                         message:String?,
                         style:UIAlertController.Style,
                         titleBlock:((_ obj:T) -> String?),
                         clickBlock:@escaping (_ alertAction:UIAlertAction,_ index:Int,_ obj:T) -> Void)

    /// 展示一个系统弹窗
    /// - Parameters:
    ///   - titles: 按钮titles对象数据
    ///   - viewCtrl: presentViewController
    ///   - view: 显示位置
    ///   - title: 标题
    ///   - message: 内容
    ///   - style: 样式
    ///   - titleBlock: titles回调 自行处理 titles中对象需要内显示的title
    ///   - clickBlock: 选项按钮点击调用
    static func show(titles:[Any],
                     viewCtrl:UIViewController,
                     view:UIView?,
                     title:String?,
                     message:String?,
                     style:UIAlertController.Style,
                     titleBlock:((_ obj:Any) -> String?),
                     clickBlock:@escaping (_ alertAction:UIAlertAction,_ index:Int,_ obj:Any) -> Void)

    /// 弹出Alert 无按钮框
    /// - Parameters:
    ///   - showTime: 当为nil时不自动隐藏  默认2s后自动关闭
    @discardableResult
    static func alert(from viewCtrl: UIViewController, title: String? = nil , message: String? = nil, showTime: TimeInterval? = 2) -> UIAlertController

#### 5、NSString<br>

    var md5: String

    var lastString: String

    var firstString: String

    func substring(from index: Int) -> String 
    
    func substring(to index: Int) -> String
    
    func substring(range index: NSRange) -> String
    
    func hidden(range index: NSRange, replace: String = "*") -> String

    func jsonFormatPrint() -> String

    func intValue() -> Int
    
    /// 判断是否是手机号码
    func isPhone() -> Bool 

    /// 是否是固话
    func isTelPhone() -> Bool 

    /// 判断是否是身份证号码
    func isIdCard() -> Bool
    
    ///判断是否是6位验证码
    func isCode_6()  -> Bool

    /// 判断字符位数
    func isLength(minLength:Int, maxLenter:Int)  -> Bool

#### 6、UIImage<br>

    /// 取图片 UIImage.init(named: name)
    static func named(name:String) -> UIImage?

    /// 取图片  named(name:String) -> UIImage?
    static func name(_ name:String) -> UIImage?

    ///base64图片
    static func base64(base64:String) -> UIImage?

    /// 使用颜色绘制一张图片 size:{1,1}
    ///
    /// - Parameter color: 将要绘制的图片颜色
    /// - Returns: 绘制完成的颜色图片
    static func image(color: UIColor) -> UIImage 

    /// 使用颜色绘制一张图片 size:{1,1}
    ///
    /// - Parameter color: 将要绘制的图片颜色
    /// - Returns: 绘制完成的颜色图片
    static func imageWithColor(_ color: UIColor) -> UIImage

    /// 使用颜色绘制一张图片
    ///
    /// - Parameters:
    ///   - color: 将要绘制的图片颜色
    ///   - size: 将要绘制的图片大小
    /// - Returns: 绘制完成的颜色图片
    static func image(color: UIColor, size: CGSize) -> UIImage? 

    /// 截取View图片
    ///
    /// - Parameters:
    ///   - fromeView: view 窗体
    ///   - atFrame: 将要截图位置及大小
    /// - Returns: 截取的图片
    static func getSnapshot(_ fromeView:UIView?,atFrame:CGRect) -> UIImage?
    
    /// 修改图片的颜色
    func imageWithColor(_ color:UIColor) -> UIImage? 

    /// 修改图片的颜色
    func imageWithTintColor(_ color:UIColor,blendMode: CGBlendMode) -> UIImage? 

    /// 图片修改成圆角
    func roundImage(byRoundingCorners: UIRectCorner = UIRectCorner.allCorners, cornerRadi: CGFloat) -> UIImage?

    /// 图片修改成圆角
    func roundImage(byRoundingCorners: UIRectCorner = UIRectCorner.allCorners, cornerRadii: CGSize) -> UIImage? 
    
    /// 水平翻转图片
    ///
    /// - Returns: 水平翻转 UIImage
    func filpImage() -> UIImage

    /// 向右旋转90° ⤼
    func byRotateRight90() -> UIImage

    /// 旋转 翻折UIImage
    func orientation(_ orientation: UIImage.Orientation) -> UIImage

    /**
     *  等比率缩放
     */
    func scaleImage(scaleSize:CGFloat) -> UIImage? 
    
    /**
     *  重设图片大小
     */
    func reSizeImage(reSize:CGSize) -> UIImage? 

    /// 压缩图片数据-不压尺寸
    ///
    /// - Parameters:
    ///   - maxLength: 最大长度
    /// - Returns:
    func compressImageOnlength(maxLength: Int) -> Data? 
    
    /// 压缩图片
    ///
    /// - Parameter maxLength: 图片压缩后最大字节
    /// - Returns: 压缩后的图片Data
    func compressImageMid(maxLength: Int) -> Data? 
    
    /// 压缩图片
    ///
    /// - Parameter maxLength: 图片压缩后最大字节
    /// - Returns: 压缩后的图片
    func compressImage(maxLength: Int) -> UIImage?

    /// 根据尺寸重新生成图片
    ///
    /// - Parameter size: 设置的大小
    /// - Returns: 新图
    func imageWithNewSize(size: CGSize) -> UIImage? 

    /// 给图片绘制文字
    func add(text:String,attributed:Dictionary<NSAttributedString.Key, Any>,scale:CGFloat) -> UIImage 

#### 7、Date<br>

    static func comps(type:Calendar.Component, date: Date = Date()) -> Int

    static func year(date: Date = Date()) -> Int

    static func month(date: Date = Date()) -> Int

    static func day(date: Date = Date()) -> Int

    static func hour(date: Date = Date()) -> Int

    static func minute(date: Date = Date()) -> Int

    static func second(date: Date = Date()) -> Int

    static func weekday(date: Date = Date()) -> Int
    
    /// 一个月有多少天
    func monthTotalDays(calendar: Calendar = Calendar.current) -> Int
    
    /// 根据date获取偏移指定天数的date year = 1表示1年后的时间 year = -1为1年前的日期，month day 类推
    func year(offset: Int, calendar: Calendar = Calendar.current) -> Date?
    
    /// 根据date获取偏移指定月数的date year = 1表示1年后的时间 year = -1为1年前的日期，month day 类推
    func month(offset: Int, calendar: Calendar = Calendar.current) -> Date?
    
    /// 根据date获取偏移指定天数的date year = 1表示1年后的时间 year = -1为1年前的日期，month day 类推
    func day(offset: Int, calendar: Calendar = Calendar.current) -> Date?
    
    /// 将Date转成字符串
    func dateString(with formatterStr: String, calendar: Calendar.Identifier? = nil) -> String
    
    /// Date转成另一种时间格式的字符串 默认"yyyy-MM-dd"
    static func date(with dateStr: String, formatter: String = "yyyy-MM-dd", calendar: Calendar.Identifier? = nil) -> Date?
    
    /// 判断两个时间年月日是否相等
    func equal(day: Date) -> Bool

    /// 年
    var year: String 
    
    /// yue
    var month: String
    
    /// 日
    var day: String 

    /// 小时
    var hour: String
    
    /// 分
    var minute: String
    
    /// 秒
    var seconds: String 
    
    /// 星期几
    var weekDay: Int
    
    /// 当前时间是否是今天
    var isToday: Bool

#### 8、UIView基础属性赋值和获取，链式编程让赋值更方便<br>
    
    var x: CGFloat 

    var y : CGFloat

    var width : CGFloat 

    var height: CGFloat 

    var maxX: CGFloat 

    var maxY: CGFloat

    var size: CGSize

    var origin : CGPoint 

    var centerX: CGFloat
    
    var centerY: CGFloat

    var borderColor: UIColor
    
    var borderWidth: CGFloat
    
    var cornerRadius: CGFloat
    
    var masksToBounds: Bool

    @discardableResult
    func border(width: CGFloat = 0.5, color: UIColor = UIColor.gray, radius: CGFloat? = nil) -> Self 

    @discardableResult func isUserInteractionEnabled(_ enable: Bool) -> Self

    @discardableResult func isHidden(_ enable: Bool) -> Self

    /// 删除所有的字窗体
    func removeAllSubViews()

    /// 删除本窗体中 选中的子窗体
    /// - Parameter subViews: 想要删除的窗体
    func remove(subViews:[UIView]) -> Void 

    /// 找到本窗体中的
    func findSubView<T: UIView>(_ Class: T.Type, resursion:Bool) -> T? 

    /// 查找当前View是否包含view
    /// - Parameter view: 需要查找的view
    @objc func isHave(view: UIView) -> Bool

    @objc private func isHave(view: UIView, in superView: UIView) -> Bool

    /// 添加一个渐变层  从startPoint->endPint   起始点x,y 在 0-1 之间
    @objc func addGradientLayer(form fColor: UIColor,to toColor: UIColor,start startPoint: CGPoint, end endPint:CGPoint)
    
    /// 添加一个渐变层  从右边->左
    @objc func addGradientLayer(form fColor: UIColor,to toColor: UIColor)

    @discardableResult
    @objc func addLine(frame:CGRect, color:UIColor) -> UIView

    @discardableResult
    @objc func addLine(_ color:UIColor = DefaultborderColor, layout:((_ view: UIView) -> ())) -> UIView 
    
    @discardableResult
    @objc func addLine(size:CGFloat,alignment:UIViewLineAlignment,color:UIColor) -> UIView

    /// 部分圆角
    ///
    /// - Parameters:
    ///   - corners: 需要实现为圆角的角，可传入多个
    ///   - radii: 圆角半径
    @discardableResult
    @objc func addRound(_ corners: UIRectCorner, radii: CGFloat) -> Self

#### 9、UIColor<br>
    
    @objc static func hexColor(hexStr:String ,alpha:CGFloat) -> UIColor 

    /// HEXColor
    @objc static func hexColor(hexStr:String) -> UIColor

    /// HEXColor
    @objc static func hexStr(_ hexStr:String) -> UIColor 

    ///  获取颜色的RGB
    func getRGB() -> (r:CGFloat,g:CGFloat,b:CGFloat,a:CGFloat)

    /// 修改颜色的透明度
    @objc static func changeAlpha(color:UIColor,alpha:CGFloat) -> UIColor 

    /// 修改颜色的透明度
    @objc func changeAlpha(_ alpha:CGFloat) -> UIColor 

#### 10、Dictionary<br>

    mutating func append(dic:Dictionary) -> Void

#### 11、UIControl 实现多个点击事件添加回调Block<br>

    /// 添加事件
    @discardableResult
    func addBlock(for events:UIControl.Event, block:@escaping ((_ sender: Any) -> Void)) -> Self

    @discardableResult
    func addTarget(for controlEvents: UIControl.Event,target: Any?, action: Selector) -> Self

    /// 删除通过block绑定的事件
    @discardableResult
    func removeBlockTarget(for events: UIControl.Event) -> Self

    /// 删除所有的点击事件
    @discardableResult
    func removeAllTarget() -> Self

    @discardableResult
    @objc dynamic func isEnabled(_ enable: Bool) -> Self

    @discardableResult
    @objc dynamic func isSelected(_ selected: Bool) -> Self

#### 12、NSObject 添加监听Observer<br>
 
    func addObservers(keyPaths:[String], key: String = "", options: NSKeyValueObservingOptions = [.old,.new], block:@escaping ObserverBlockTargetAlias)

    func addObserver(keyPath: String, key: String = "", options: NSKeyValueObservingOptions = [.old,.new], block:@escaping ObserverBlockTargetAlias)

    func removeObserver(keyPath: String, key: String)

    func removeObserver(keyPaths: [String], key: String)

    func removeObserver(keyPath: String)

    func remoAllObservers()

#### 13、CALayer<br>

    /// 删除所有的子Layer
    func removeAllSubLayers()

    var size: CGSize

    var center: CGPoint

#### 14、UILabel 链式编程方法添加<br>

    var lineHeight: CGFloat

    @discardableResult
    func lineHeight(_ value: CGFloat) -> Self

    @discardableResult
    func font(_ font: UIFont) -> Self

    @discardableResult
    func attributedText(_ value: NSAttributedString?) -> Self

    @discardableResult
    func numberOfLines(_ value: Int) -> Self

    @discardableResult
    func lineBreakMode(_ value: NSLineBreakMode) -> Self

    @discardableResult
    func textColor(_ color: UIColor) -> Self

    @discardableResult
    func textAlignment(_ alignment: NSTextAlignment) -> Self
    
    @discardableResult
    func text(_ text: String?) -> Self

#### 15、UITableView 实现cell，header，footer快速注册及获取<br>

    @discardableResult
    func register<T>(cell cellClass: T.Type, nibName: String? = nil, identifier: String = "\(NSStringFromClass(T.self))_identifier") -> Self
    
    @discardableResult
    func register<T>(headerFooter cellClass: T.Type, nibName: String? = nil, identifier: String = "\(NSStringFromClass(T.self))_identifier") -> Self
    
    func defaultCell(_ style:UITableViewCell.CellStyle = .default, initBlock: ((_ cell: UITableViewCell) -> Void)? = nil) -> UITableViewCell 
    
    func cellT<T: UITableViewCell>(identifier: String = "\(NSStringFromClass(T.self))_identifier",
                  cellClass:T.Type,
                  for indexPath: IndexPath) -> T

    func cellT<T: UITableViewCell>(identifier: String = "\(NSStringFromClass(T.self))_identifier",
                  cellClass:T.Type,
                  style:UITableViewCell.CellStyle = .default,
                  initBlock:((_ cell:T) -> Void)? = nil) -> T

    func headerFooterView<T: UITableViewHeaderFooterView>(identifier: String = "\(NSStringFromClass(T.self))HeaderFooter_identifier",
        headerFooterClass: T.Type,
        initBlock:((_ cell:T) -> Void)? = nil) -> T

#### 16、UICollectionView  实现cell，header，footer快速注册及获取<br>

    @discardableResult
    func register<T>(cell cellClass: T.Type, nibName: String? = nil, identifier: String = "\(NSStringFromClass(T.self))_identifier") -> Self 
    
    @discardableResult
    func register<T>(header headerClass: T.Type, nibName: String? = nil, identifier: String = "\(NSStringFromClass(T.self))_identifier") -> Self
    
    @discardableResult
    func register<T>(footer headerClass: T.Type, nibName: String? = nil, identifier: String = "\(NSStringFromClass(T.self))_identifier") -> Self

    func cell<T: UICollectionViewCell>(_ identifier: String = "\(NSStringFromClass(T.self))_identifier",
                 cellClass: T.Type,
                 indexPath: IndexPath) -> T
    
    func dequeueReusableSupplementaryView<T: UICollectionReusableView>(ofKind elementKind: String, withReuseIdentifier identifier: String = "\(NSStringFromClass(T.self))_identifier", for indexPath: IndexPath, classType: T.Type) -> T

    func headerView<T: UICollectionReusableView>(_ classType: T.Type, for indexPath: IndexPath, identifier: String = "\(NSStringFromClass(T.self))_identifier") -> T

    func footerView<T: UICollectionReusableView>(_ classType: T.Type, for indexPath: IndexPath, identifier: String = "\(NSStringFromClass(T.self))_identifier") -> T

### 自定义组件

#### 1、ZZScrollView<br>
![img](https://gitee.com/czz_8023/ZZToolKit/raw/master/Gif/ZZScrollView.gif)

ZZScrollView继承至UIScrollView，实现可将任意自定义组件串联，并流畅的上下滑动。比如：UIView + UITableView + UIView + WKWebView + UIScrollView + UICollectionView 等 任意组合。实用中如新闻详情页（上部为新闻台头，中部为web页面的新闻类容，底部是评论）都可快速的实现，不用再去计算对应组件的滚动高度。<br>
使用方式：<br>
        
        let customItem1 = ZZScrollView.Item(view: customView1, maxHeight: 268)
        let customItem2 = ZZScrollView.Item(view: customView2)
        
        let scrollItem1 = ZZScrollView.Item(view: scrollView1)
        let scrollItem2 = ZZScrollView.Item(view: scrollView2)
        
        let webItem1 = ZZScrollView.Item(view: webView1)
        let webItem2 = ZZScrollView.Item(view: webView3)
        
        let zzScrollView = ZZScrollView(items: [customItem1, scrollItem1, webItem1, webItem2, scrollItem2, customItem2])

ZZScrollView.Item <br>

        private(set) public var minHeight: CGFloat = 0
        private(set) public var maxHeight: CGFloat = 0 // 当view 不为 uiscorollview及其子类时生效
        private(set) public var fixedWidth: CGFloat = 0 // 与inset 冲突  当大于0 时 inset left right 失效
        private(set) public var view: UIView!
        @objc public dynamic var inset: UIEdgeInsets = .zero
        @objc private(set) public dynamic var contentSize: CGSize = .zero
        @objc private(set) public dynamic var isHidden: Bool = false

#### 2、ZZUIButton<br>
![img](https://gitee.com/czz_8023/ZZToolKit/raw/master/Gif/ZZUIButton.gif)

ContentAlignment<br>

![img](https://gitee.com/czz_8023/ZZToolKit/raw/master/Gif/ContentAlignment2.jpg)
![img](https://gitee.com/czz_8023/ZZToolKit/raw/master/Gif/ContentAlignment1.jpg)

ImageAlignment<br>

![img](https://gitee.com/czz_8023/ZZToolKit/raw/master/Gif/ImageAlignment1.jpg)
![img](https://gitee.com/czz_8023/ZZToolKit/raw/master/Gif/ImageAlignment2.jpg)

ZZUIButton可以随意设置图片位置和内容对其方式，可任意将图片相对内容各个位置对其。<br>

使用方式：<br>

    let button = ZZUIButton()
        button.backgroundColor(color: .red, state: .normal)
            .backgroundColor(color: .blue, state: .highlighted)
            .backgroundColor(color: .orange, state: .selected)
            .backgroundColor(color: .yellow, state: .disable)
            .title(title: "normal", state: .normal)
            .title(title: "highlighted", state: .highlighted)
            .title(title: "selected", state: .selected)
            .title(title: "disable", state: .disable)
            .image(image: UIImage.name("icon_timeout"), state: .normal)
            .title(title: title, state: .normal)
            .contentAlignment(contentAlignment)// 内容对其
            .imageAlignment(imageAlignment) // image相对内容对其
            .space(20)
            .contentViewBgColor(color: .green, state: .normal)
            .contentInset(UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))

#### 3、ZZPageControl<br>

![img](https://gitee.com/czz_8023/ZZToolKit/raw/master/Gif/ZZPageControl.png)

使用方式与系统UIPageControl类似，在系统UIPageContrl上添加了对其方式，更方便的图片设置及页码获取<br>

    @IBInspectable open var numberOfPages: Int = 0

    @IBInspectable open var currentPage: Int = 0

    @IBInspectable open var pageImage: UIImage?

    @IBInspectable open var currentPageImage: UIImage?
    
    @IBInspectable open var alignment: PageControlAlignment = .Center

    @IBInspectable open var pageIndicatorTintColor: UIColor?

    @IBInspectable open var currentPageIndicatorTintColor: UIColor?

    @IBInspectable open var pageSpace: CGFloat = 6

对其方式：<br>

    enum PageControlAlignment : NSInteger{
            case Center,Left,Right
        }

使用方式：<br>

    let view = ZZPageControl()
            view.numberOfPages = 8
            view.currentPage = 0
            view.pageImage = pageImage
            view.currentPageImage = pageImage
            view.alignment = alignment
            view.pageIndicatorTintColor = pageIndicatorTintColor
            view.currentPageIndicatorTintColor = currentPageIndicatorTintColor
            view.pageSpace = 10
            view.indicatorSize = CGSize(width: 20, height: 15)

#### 4、ZZBannerView<br>

![img](https://gitee.com/czz_8023/ZZToolKit/raw/master/Gif/ZZBannerView.gif)

一个Banner展示View，可循环滚动和添加自定义View<br>

使用方式：<br>

    let view = ZZBannerView()
        view.createBannerImagesBlock = { page, imageView, object in
            imageView.backgroundColor = UIColor.lightGray
            imageView.cornerRadius(10)
            imageView.contentMode = .scaleAspectFill
        }
        view.isAutoRun = true
        view.isCycleRunBanner = true
        view.masksToBounds(true)




